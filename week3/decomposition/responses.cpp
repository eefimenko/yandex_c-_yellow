#include "responses.h"

ostream& operator << (ostream& os, const BusesForStopResponse& r) {
  // Реализуйте эту функцию
    if (r.buses.size() == 0)
    {
	os << "No stop";
    }
    else
    {
	for (const string& bus : r.buses) {
          os << bus << " ";
        }
//        os << endl;
    }
  return os;
}

ostream& operator << (ostream& os, const StopsForBusResponse& r) {
  // Реализуйте эту функцию
    int n = r.stops.size();
    if (n == 0)
    {
	os << "No bus";
    }
    else {
//        for (const string& stop : r.stops) {
	for (int i = 0; i < n; i++) {
	    const string& stop = r.stops.at(i);
          os << "Stop " << stop << ": ";
          if (r.interchange.at(stop).size() == 0) {
            os << "no interchange";
          } else {
	      for (const string& bus : r.interchange.at(stop)) {
		os << bus << " ";
	    }
	  }
	  if (i != n-1) {
	      os << endl;
	  }
        }
      }
  return os;
}

ostream& operator << (ostream& os, const AllBusesResponse& r) {
  // Реализуйте эту функцию
    int n = r.buses.size();
    if (r.buses.empty()) {
        os << "No buses";
      } else {
	int i = 0;
        for (const auto& bus_item : r.buses) {
	    os << "Bus " << bus_item.first << ": ";
          for (const string& stop : bus_item.second) {
            os << stop << " ";
          }
	  if (i != n-1) {
	      os << endl;
	  }
	  i++;
        }
      }
  return os;
}
