#include <iostream>
#include <string>
#include <vector>
#include "phone_number.h"

using namespace std;

int main() {
    PhoneNumber pn("+7-88-295-69-34");
    cout << pn.GetInternationalNumber() << endl;
    return 0;
}
