#include "phone_number.h"
#include <stdexcept>
#include <sstream>
#include <iostream>
#include <vector>

PhoneNumber::PhoneNumber(const string &international_number)
{
    if (international_number.size() == 0)
	throw invalid_argument("Empty number");
    if (international_number[0] != '+')
	throw invalid_argument("No + at the beginning");

    string temp = international_number;
    temp.erase(0, 1);
    stringstream test(temp);
    string segment;
    vector<string> seglist;

    while(getline(test, segment, '-'))
    {
	seglist.push_back(segment);
	if (segment.size() == 0)
	    throw invalid_argument("Empty part");
//	cout << segment << endl;
    }
    if (seglist.size() < 3)
	throw invalid_argument("Wrong number of parts");
    country_code_ = seglist[0];
    city_code_ = seglist[1];
    local_number_ = seglist[2];
    for (uint i=0; i < seglist.size()-3; i++)
    {
	local_number_ += "-" + seglist[3+i];
    }
	
}

string PhoneNumber::GetCountryCode() const
{
    return country_code_;
}

string PhoneNumber::GetCityCode() const
{
    return city_code_;
}
string PhoneNumber::GetLocalNumber() const
{
    return local_number_;
}
string PhoneNumber::GetInternationalNumber() const
{
    return "+" + country_code_ + "-" + city_code_ + "-" + local_number_;
}
