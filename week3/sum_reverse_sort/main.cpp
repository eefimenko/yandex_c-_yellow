#include <iostream>
#include <string>
#include <vector>
#include "sum_reverse_sort.h"

using namespace std;

int main() {
    int x = 1, y = 2;
    string s = "string";
    vector<int> nums = {1,6,3,5,3};
    cout << x << " " << y << " " << Sum(x,y) << endl;
    cout << s << " " << Reverse(s) << endl;
    for (const auto& el : nums)
	cout << el << " ";
    cout << endl;
    Sort(nums);
    for (const auto& el : nums)
	cout << el << " ";
    cout << endl;
    return 0;
}
