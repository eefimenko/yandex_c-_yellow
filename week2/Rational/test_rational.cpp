#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;

template <class T>
ostream& operator << (ostream& os, const vector<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class T>
ostream& operator << (ostream& os, const set<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class K, class V>
ostream& operator << (ostream& os, const map<K, V>& m) {
  os << "{";
  bool first = true;
  for (const auto& kv : m) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << kv.first << ": " << kv.second;
  }
  return os << "}";
}

template<class T, class U>
void AssertEqual(const T& t, const U& u, const string& hint = {}) {
  if (t != u) {
    ostringstream os;
    os << "Assertion failed: " << t << " != " << u;
    if (!hint.empty()) {
       os << " hint: " << hint;
    }
    throw runtime_error(os.str());
  }
}

void Assert(bool b, const string& hint) {
  AssertEqual(b, true, hint);
}

class TestRunner {
public:
  template <class TestFunc>
  void RunTest(TestFunc func, const string& test_name) {
    try {
      func();
      cerr << test_name << " OK" << endl;
    } catch (exception& e) {
      ++fail_count;
      cerr << test_name << " fail: " << e.what() << endl;
    } catch (...) {
      ++fail_count;
      cerr << "Unknown exception caught" << endl;
    }
  }

  ~TestRunner() {
    if (fail_count > 0) {
      cerr << fail_count << " unit tests failed. Terminate" << endl;
      exit(1);
    }
  }

private:
  int fail_count = 0;
};

/*class Rational {
public:
  // Вы можете вставлять сюда различные реализации,
  // чтобы проверить, что ваши тесты пропускают корректный код
  // и ловят некорректный

  Rational()
    {
	numerator_ = 0;
	denominator_ = 1;
    };
  Rational(int numerator, int denominator) {
      if (numerator == 0)
      {
	  numerator_ = 0;
	  denominator_ = 1;
	  return;
      }
      int sign = numerator*denominator/(abs(numerator)*abs(denominator));
      int a = abs(numerator);
      int b = abs(denominator);
      
      while (a!=0 && b!=0)
      {
	  if (a > b)
	      a = a % b;
	  else
	      b = b % a;
      }
      int nod = a+b;
      numerator_ = sign*abs(numerator)/nod;
      denominator_ = abs(denominator)/nod;
      cout << numerator << " " << denominator << " " << numerator_ << " " << denominator_ << endl;
  }

  int Numerator() const {
      return numerator_;
  }

  int Denominator() const {
      return denominator_;
  }
private:
    int numerator_;
    int denominator_;
    };*/

void TestDefault()
{
    AssertEqual(Rational().Numerator(), 0);
    AssertEqual(Rational().Denominator(), 1);
}

void TestConstructorWithZeroNominator()
{
    AssertEqual(Rational(0,-1000).Numerator(), 0);
    AssertEqual(Rational(0,-1000).Denominator(), 1);
}

void TestConstructorWithNegativeNominatorAndDenominator()
{
    AssertEqual(Rational(-1000,-1000).Numerator(), 1);
    AssertEqual(Rational(-1000,-1000).Denominator(), 1);
}

void TestConstructorWithNod()
{
    AssertEqual(Rational(4,6).Numerator(), 2);
    AssertEqual(Rational(4,6).Denominator(), 3);
    AssertEqual(Rational(4,-6).Numerator(), -2);
    AssertEqual(Rational(4,-6).Denominator(), 3);
    AssertEqual(Rational(-4,6).Numerator(), -2);
    AssertEqual(Rational(-4,6).Denominator(), 3);
}

int main() {
  TestRunner runner;
  runner.RunTest(TestDefault, "default constructor");
  runner.RunTest(TestConstructorWithZeroNominator, "zero nominator ");
  runner.RunTest(TestConstructorWithNegativeNominatorAndDenominator, "negative nominator and denominator");
  runner.RunTest(TestConstructorWithNod, "general case");
  // добавьте сюда свои тесты
  return 0;
}
