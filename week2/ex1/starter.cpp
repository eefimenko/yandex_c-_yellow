#include <string>
#include <iostream>
#include <cassert>
#include <vector>
#include <map>

using namespace std;

enum class QueryType {
  NewBus,
  BusesForStop,
  StopsForBus,
  AllBuses
};

struct Query {
  QueryType type;
  string bus;
  string stop;
  vector<string> stops;
};

istream& operator >> (istream& is, Query& q) {
  // Реализуйте эту функцию
    string operation_code;
    is >> operation_code;
    if (operation_code == "NEW_BUS") {
	q.type = QueryType::NewBus;
	is >> q.bus;
	int stop_count;
	is >> stop_count;
	q.stops.resize(stop_count);
	for (string& stop: q.stops)
	{
	    is >> stop;  
	}
    }
    else if (operation_code == "BUSES_FOR_STOP") {
	q.type = QueryType::BusesForStop;
	is >> q.stop;
    }
    else if (operation_code == "STOPS_FOR_BUS") {
	q.type = QueryType::StopsForBus;
	is >> q.bus;
    }
    else if (operation_code == "ALL_BUSES") {
	q.type = QueryType::AllBuses;
    }
  return is;
}

struct BusesForStopResponse {
  // Наполните полями эту структуру
    vector<string> buses;
};

ostream& operator << (ostream& os, const BusesForStopResponse& r) {
  // Реализуйте эту функцию
    if (r.buses.size() == 0)
    {
	os << "No stop";
    }
    else
    {
	for (const string& bus : r.buses) {
          os << bus << " ";
        }
//        os << endl;
    }
  return os;
}

struct StopsForBusResponse {
    // Наполните полями эту структуру
    vector<string> stops;
    map<string,vector<string>> interchange;
};

ostream& operator << (ostream& os, const StopsForBusResponse& r) {
  // Реализуйте эту функцию
    int n = r.stops.size();
    if (n == 0)
    {
	os << "No bus";
    }
    else {
//        for (const string& stop : r.stops) {
	for (int i = 0; i < n; i++) {
	    const string& stop = r.stops.at(i);
          os << "Stop " << stop << ": ";
          if (r.interchange.at(stop).size() == 0) {
            os << "no interchange";
          } else {
	      for (const string& bus : r.interchange.at(stop)) {
		os << bus << " ";
	    }
	  }
	  if (i != n-1) {
	      os << endl;
	  }
        }
      }
  return os;
}

struct AllBusesResponse {
  // Наполните полями эту структуру
    map<string,vector<string>> buses;
};

ostream& operator << (ostream& os, const AllBusesResponse& r) {
  // Реализуйте эту функцию
    int n = r.buses.size();
    if (r.buses.empty()) {
        os << "No buses";
      } else {
	int i = 0;
        for (const auto& bus_item : r.buses) {
	    os << "Bus " << bus_item.first << ": ";
          for (const string& stop : bus_item.second) {
            os << stop << " ";
          }
	  if (i != n-1) {
	      os << endl;
	  }
	  i++;
        }
      }
  return os;
}

class BusManager {
public:
  void AddBus(const string& bus, const vector<string>& stops) {
    // Реализуйте этот метод
      for (const string& stop : stops) {
	  buses_to_stops[bus].push_back(stop);
	  stops_to_buses[stop].push_back(bus);
      }
  }

    BusesForStopResponse GetBusesForStop(const string& stop) const {
	BusesForStopResponse r;
	if (stops_to_buses.count(stop) > 0) {
	    for (const string& bus : stops_to_buses.at(stop)) {
		r.buses.push_back(bus);
	    }
        }
	return r;
    // Реализуйте этот метод
  }

  StopsForBusResponse GetStopsForBus(const string& bus) const {
    // Реализуйте этот метод
      StopsForBusResponse r;
       if (buses_to_stops.count(bus) > 0) {
	   for (const string& stop : buses_to_stops.at(bus)) {
	       r.stops.push_back(stop);
	       r.interchange[stop];
	       //cout << "Stop " << stop << "size=" << stops_to_buses.at(stop).size()<< endl;
	       if (stops_to_buses.at(stop).size() > 1) {
		   //cout << "Stop " << stop << "size=" << stops_to_buses.at(stop).size()<< endl;
		   for (const string& other_bus : stops_to_buses.at(stop)) {
		       if (bus != other_bus) {
			   r.interchange[stop].push_back(other_bus);
		       }
		   }
	       }
	   }
       }
       return r;
  }

  AllBusesResponse GetAllBuses() const {
      AllBusesResponse r;
      if (!buses_to_stops.empty()) {
	  for (const auto& bus_item : buses_to_stops) {
	      for (const string& stop : bus_item.second) {
		  r.buses[bus_item.first].push_back(stop);
	      }
	  }
      }
      return r;
      // Реализуйте этот метод
  }
private:
    map<string, vector<string>> buses_to_stops, stops_to_buses;
};

// Не меняя тела функции main, реализуйте функции и классы выше

int main() {
  int query_count;
  Query q;

  cin >> query_count;

  BusManager bm;
  for (int i = 0; i < query_count; ++i) {
    cin >> q;
    switch (q.type) {
    case QueryType::NewBus:
      bm.AddBus(q.bus, q.stops);
      break;
    case QueryType::BusesForStop:
      cout << bm.GetBusesForStop(q.stop) << endl;
      break;
    case QueryType::StopsForBus:
      cout << bm.GetStopsForBus(q.bus) << endl;
      break;
    case QueryType::AllBuses:
      cout << bm.GetAllBuses() << endl;
      break;
    }
  }

  return 0;
}
