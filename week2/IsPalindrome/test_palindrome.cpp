#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;

template <class T>
ostream& operator << (ostream& os, const vector<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class T>
ostream& operator << (ostream& os, const set<T>& s) {
  os << "{";
  bool first = true;
  for (const auto& x : s) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << x;
  }
  return os << "}";
}

template <class K, class V>
ostream& operator << (ostream& os, const map<K, V>& m) {
  os << "{";
  bool first = true;
  for (const auto& kv : m) {
    if (!first) {
      os << ", ";
    }
    first = false;
    os << kv.first << ": " << kv.second;
  }
  return os << "}";
}

template<class T, class U>
void AssertEqual(const T& t, const U& u, const string& hint = {}) {
  if (t != u) {
    ostringstream os;
    os << "Assertion failed: " << t << " != " << u;
    if (!hint.empty()) {
       os << " hint: " << hint;
    }
    throw runtime_error(os.str());
  }
}

void Assert(bool b, const string& hint) {
  AssertEqual(b, true, hint);
}

class TestRunner {
public:
  template <class TestFunc>
  void RunTest(TestFunc func, const string& test_name) {
    try {
      func();
      cerr << test_name << " OK" << endl;
    } catch (exception& e) {
      ++fail_count;
      cerr << test_name << " fail: " << e.what() << endl;
    } catch (...) {
      ++fail_count;
      cerr << "Unknown exception caught" << endl;
    }
  }

  ~TestRunner() {
    if (fail_count > 0) {
      cerr << fail_count << " unit tests failed. Terminate" << endl;
      exit(1);
    }
  }

private:
  int fail_count = 0;
};

void TestTrivial()
{
    AssertEqual(IsPalindrom(""), true);
    AssertEqual(IsPalindrom("b"), true);
}

void TestNoSpaces()
{
    AssertEqual(IsPalindrom("aa"), true);
    AssertEqual(IsPalindrom("abba"), true);
    AssertEqual(IsPalindrom("abab"), false);
}

void TestWithSpaces()
{
    AssertEqual(IsPalindrom(" "), true);
    AssertEqual(IsPalindrom("  "), true);
    AssertEqual(IsPalindrom(" g "), true);
    AssertEqual(IsPalindrom("a a"), true);
    AssertEqual(IsPalindrom("a bb a"), true);
    AssertEqual(IsPalindrom("ab ab"), false);
    AssertEqual(IsPalindrom("aa "), false);
    AssertEqual(IsPalindrom(" aa"), false);
}

void TestWrongImplementation()
{
    AssertEqual(IsPalindrom("aab"), false);
    AssertEqual(IsPalindrom("abb"), false);
    AssertEqual(IsPalindrom("aabbaa"), true);
    AssertEqual(IsPalindrom("aabaaa"), false);
    AssertEqual(IsPalindrom("aabaa"), true);
}

/*bool IsPalindrom(const string& str) {
  // Вы можете вставлять сюда различные реализации функции,
  // чтобы проверить, что ваши тесты пропускают корректный код
  // и ловят некорректный
  }*/

int main() {
  TestRunner runner;
  runner.RunTest(TestTrivial, "trivial test");
  runner.RunTest(TestNoSpaces, "no spaces");
  runner.RunTest(TestWithSpaces, "with spaces");
  runner.RunTest(TestWrongImplementation, "wrong implmentation");
  // добавьте сюда свои тесты
  return 0;
}
