#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <tuple>

using namespace std;

/*enum class TaskStatus {
  NEW,          // новая
  IN_PROGRESS,  // в разработке
  TESTING,      // на тестировании
  DONE          // завершена
};

// Объявляем тип-синоним для map<TaskStatus, int>,
// позволяющего хранить количество задач каждого статуса
using TasksInfo = map<TaskStatus, int>;*/

void PrintTasksInfo(TasksInfo tasks_info) {
  cout << tasks_info[TaskStatus::NEW] << " new tasks" <<
      ", " << tasks_info[TaskStatus::IN_PROGRESS] << " tasks in progress" <<
      ", " << tasks_info[TaskStatus::TESTING] << " tasks are being tested" <<
      ", " << tasks_info[TaskStatus::DONE] << " tasks are done" << endl;
}

class TeamTasks {
public:
  // Получить статистику по статусам задач конкретного разработчика
  const TasksInfo& GetPersonTasksInfo(const string& person) const
    {
	return db.at(person);
    };
  
  // Добавить новую задачу (в статусе NEW) для конкретного разработчитка
  void AddNewTask(const string& person)
    {
	db[person][TaskStatus::NEW]++;
    };
    void transferTasks(const string& person, TasksInfo& changed, TasksInfo& unchanged, TasksInfo& out, TaskStatus inStatus, TaskStatus outStatus, int& count)
    {
	int db_count = 0;
	try
	{
	    db_count = db[person].at(inStatus);
	}
	catch(...)
	{
	    return;
	}
	//cout << "0. db_count=" << db_count << " count=" << count << endl; 
	if (count == 0)
	{
	    unchanged[inStatus] = db_count;
	    return;
	}
	
	if (count < db_count)
	{
	    //cout << "1. db_count=" << db_count << " count=" << count << endl; 
	    unchanged[inStatus] = db_count - count;
	    changed[outStatus] = count;
	    out[inStatus] -= count;
	    out[outStatus] += count;
	    //cout << "out " << out[outStatus] << endl; 
	    count = 0;
	}
	else
	{
	    //cout << "2. db_count=" << db_count << " count=" << count << endl; 
	    out[outStatus] += db_count;
	    out[inStatus] -= db_count;
	    //cout << "out " << out[outStatus] << endl; 
	    changed[outStatus] = db_count;
	    count -= db_count;
	    if (count < 0)
	    {
		count = 0;
	    }
	}
    };
    
    void copyDataFromTempDb(const string& person, const TasksInfo& tmp, TaskStatus status)
    {
	if (tmp.count(status) > 0)
	{
	    //int count = tmp.at(status);
	    //cout << "Transfer " << static_cast<int>(status) << ": " << count << endl;
	    db[person][status] = tmp.at(status);
	}
	else
	{
	    //cout << "Delete " << static_cast<int>(status) << endl;
	    db[person].erase(status);
	}
    };
  // Обновить статусы по данному количеству задач конкретного разработчика,
  // подробности см. ниже
  tuple<TasksInfo, TasksInfo> PerformPersonTasks(
      const string& person, int task_count)
    {
	TasksInfo tempTasks = db[person];
	TasksInfo changed, unchanged;
	/*cout << "*0---------" << endl;
	PrintTasksInfo(tempTasks);
	cout << "*0---------" << endl;
	cout << "**0---------" << endl;
	PrintTasksInfo(db[person]);
	cout << "**0---------" << endl;*/
	transferTasks(person, changed, unchanged, tempTasks, TaskStatus::NEW, TaskStatus::IN_PROGRESS, task_count);
	/*cout << "*1---------" << endl;
	PrintTasksInfo(tempTasks);
	cout << "*1---------" << endl;*/
	transferTasks(person, changed, unchanged, tempTasks, TaskStatus::IN_PROGRESS, TaskStatus::TESTING, task_count);
	/*cout << "*2---------" << endl;
	PrintTasksInfo(tempTasks);
	cout << "*2---------" << endl;*/
	transferTasks(person, changed, unchanged, tempTasks, TaskStatus::TESTING, TaskStatus::DONE, task_count);
	/*cout << "*3---------" << endl;
	PrintTasksInfo(tempTasks);
	cout << "*3---------" << endl;*/
	//tempTasks[TaskStatus::DONE] += db[person][TaskStatus::DONE];
	//unchanged[TaskStatus::DONE] = db[person][TaskStatus::DONE];
	//cout << "*---------" << endl;
	//PrintTasksInfo(tempTasks);
	//cout << "*---------" << endl;
	copyDataFromTempDb(person, tempTasks, TaskStatus::NEW);
	copyDataFromTempDb(person, tempTasks, TaskStatus::IN_PROGRESS);
	copyDataFromTempDb(person, tempTasks, TaskStatus::TESTING);
	copyDataFromTempDb(person, tempTasks, TaskStatus::DONE);
	return make_tuple(changed, unchanged);
    };
    
private:
    map<string, TasksInfo> db;
};




/*int main() {
  TeamTasks tasks;
  tasks.AddNewTask("Ilia");
  for (int i = 0; i < 3; ++i) {
    tasks.AddNewTask("Ivan");
  }
  cout << "Ilia's tasks: ";
  PrintTasksInfo(tasks.GetPersonTasksInfo("Ilia"));
  cout << "Ivan's tasks: ";
  PrintTasksInfo(tasks.GetPersonTasksInfo("Ivan"));
  
  TasksInfo updated_tasks, untouched_tasks;
  
  tie(updated_tasks, untouched_tasks) =
      tasks.PerformPersonTasks("Ivan", 2);
  cout << "Updated Ivan's tasks: ";
  PrintTasksInfo(updated_tasks);
  cout << "Untouched Ivan's tasks: ";
  PrintTasksInfo(untouched_tasks);
  cout << "---------" << endl;
  PrintTasksInfo(tasks.GetPersonTasksInfo("Ivan"));
  cout << "---------" << endl;
  tie(updated_tasks, untouched_tasks) =
      tasks.PerformPersonTasks("Ivan", 2);
  cout << "Updated Ivan's tasks: ";
  PrintTasksInfo(updated_tasks);
  cout << "Untouched Ivan's tasks: ";
  PrintTasksInfo(untouched_tasks);
  for (int i = 0; i < 5; ++i) {
      tasks.AddNewTask("Alice");
  }
  cout << "---------" << endl;
  PrintTasksInfo(tasks.GetPersonTasksInfo("Alice"));
  cout << "---------" << endl;
  tie(updated_tasks, untouched_tasks) =
      tasks.PerformPersonTasks("Alice", 5);
  cout << "Updated Alice's tasks: ";
  PrintTasksInfo(updated_tasks);
  cout << "Untouched Alice's tasks: ";
  PrintTasksInfo(untouched_tasks);
  tie(updated_tasks, untouched_tasks) =
      tasks.PerformPersonTasks("Alice", 5);
  cout << "Updated Alice's tasks: ";
  PrintTasksInfo(updated_tasks);
  cout << "Untouched Alice's tasks: ";
  PrintTasksInfo(untouched_tasks);
  tie(updated_tasks, untouched_tasks) =
      tasks.PerformPersonTasks("Alice", 1);
  cout << "---------" << endl;
  PrintTasksInfo(tasks.GetPersonTasksInfo("Alice"));
  cout << "---------" << endl;
  for (int i = 0; i < 5; ++i) {
      tasks.AddNewTask("Alice");
  }
  cout << "---------" << endl;
  PrintTasksInfo(tasks.GetPersonTasksInfo("Alice"));
  cout << "---------" << endl;
  tie(updated_tasks, untouched_tasks) =
      tasks.PerformPersonTasks("Alice", 2);
  cout << "Updated Alice's tasks: ";
  PrintTasksInfo(updated_tasks);
  cout << "Untouched Alice's tasks: ";
  PrintTasksInfo(untouched_tasks);
  cout << "---------" << endl;
  PrintTasksInfo(tasks.GetPersonTasksInfo("Alice"));
  cout << "---------" << endl;
  tie(updated_tasks, untouched_tasks) =
      tasks.PerformPersonTasks("Alice", 4);
  cout << "Updated Alice's tasks: ";
  PrintTasksInfo(updated_tasks);
  cout << "Untouched Alice's tasks: ";
  PrintTasksInfo(untouched_tasks);
  cout << "---------" << endl;
  PrintTasksInfo(tasks.GetPersonTasksInfo("Alice"));
  cout << "---------" << endl;
  return 0;
  }*/



