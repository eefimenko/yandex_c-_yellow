#include <iostream>
#include <vector>
#include <map>


using namespace std;

template<typename Key, typename Value>
Value& GetRefStrict(map<Key,Value>& m, Key k)
{
    if (m.count(k) > 0)
    {
	return m.at(k);
    }
    throw runtime_error("No key");
};

/*int main() {
    map<int, string> m = {{0, "value"}};
    string& item = GetRefStrict(m, 0);
    item = "newvalue";
    cout << m[0] << endl; // выведет newvalue
    }*/
