#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

int main() {
    int N;
    int density; 
    cin >> N >> density;
    vector<int64_t> a(N), b(N), c(N);
        
    for (int i=0; i<N; ++i)
    {
	cin >> a[i] >> b[i] >> c[i];
    }

    uint64_t v = 0;
  
    for (int i=0; i<N; i++)
    {
	v += a[i]*b[i]*c[i];
    }
    cout << v*density << endl;
   
    cout << endl;
}
