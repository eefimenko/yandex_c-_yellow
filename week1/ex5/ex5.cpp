#include <iostream>
#include <vector>
#include <map>

using namespace std;
template<typename First, typename Second> pair<First, Second> Sqr(const pair<First, Second>& v);
template<typename T> vector<T> Sqr(const vector<T>& v);
template<typename Key, typename Value> map<Key, Value> Sqr(const map<Key, Value>& v);
template<typename T> T Sqr(const T& v);

template<typename T>
T Sqr(const T& v)
{
    return v*v;
}

template<typename T>
vector<T> Sqr(const vector<T>& v)
{
    vector<T> tmp;
    for (const auto& e: v)
    {
	tmp.push_back(Sqr(e));
    }
    return tmp;
}

template<typename Key, typename Value>
map<Key, Value> Sqr(const map<Key, Value>& v)
{
    map<Key, Value> tmp;
    for (const auto& e: v)
    {
	tmp[e.first] = Sqr(e.second);
    }
    return tmp;
}

template<typename First, typename Second>
pair<First, Second> Sqr(const pair<First, Second>& v)
{
    return make_pair(Sqr(v.first), Sqr(v.second));
}

/*int main() {
    vector<int> v = {1,2,4};
    cout << "vector:";
    for (int x : Sqr(v)) {
	cout << ' ' << x;
    }
    map<int, pair<int, int>> map_of_pairs = {
	{4, {2, 2}},
	{7, {4, 3}}
    };
    cout << "map of pairs:" << endl;
    for (const auto& x : Sqr(map_of_pairs)) {
	cout << x.first << ' ' << x.second.first << ' ' << x.second.second << endl;
    }
    cout << endl;
    }*/
