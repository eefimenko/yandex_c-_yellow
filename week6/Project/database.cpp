#include "database.h"
#include <algorithm>

void Database::Add(const Date& date, const string& event)
{
    if (storage[date].find(event) == end(storage[date]))
    {
	history[date].push_back(event);
	storage[date].insert(event);
    }
}

void Database::Print(ostream& out) const
{
    for (const auto& p: history)
    {
	const string& date = p.first.toString();
	for (const auto& v: p.second)
	{
	    out << date << " " << v << endl;
	}
    }
}

int Database::RemoveIf(function<bool(const Date& date, const string& event)> lambda)
{
    int n = 0;

    for (auto it0 = begin(storage); it0 != end(storage);)
    {
	const auto& date = it0->first;
	auto& events = it0->second;
	
	for (auto it = begin(events); it != end(events);)
	{
	    auto event = *it;
	    if (lambda(date, event))
	    {
		history[date].erase(remove(begin(history[date]),end(history[date]),event),
				    end(history[date]));

		it = events.erase(it);
		n++;
	    }
	    else
	    {
		it++;
	    }
	}

	if ((storage[date].empty() && !history[date].empty()) ||
	    (!storage[date].empty() && history[date].empty()))
	{
	    cerr << "Something wrong here..." << endl;
	    throw logic_error("Something wrong here...");
	}
	if (storage[date].size() != history[date].size())
	{
	    cerr << "Size wrong here..." << endl;
	    throw logic_error("Size wrong here...");
	}
	
	if (history[date].empty())
	{
	    history.erase(date);
	}
	if (storage[date].empty())
	{
	    it0 = storage.erase(it0);
	}
	else
	{
	    it0++;
	}
	if (storage.size() != history.size())
	{
	    cerr << "Size wrong here..." << endl;
	    throw logic_error("storage.size() != history.size()");
	}
    }
    return n;
}

vector<string> Database::FindIf(function<bool(const Date& date, const string& event)> lambda) const
{
    vector<string> res;
    for (const auto& p: history)
    {
	const auto& date = p.first;
	const auto& events = p.second;

	for (const auto& event: events)
	{
	    if (lambda(date, event))
	    {
		res.push_back(date.toString() + " " + event);
	    }
	}
    }
    return res;
}

string Database::Last(const Date& date) const
{
//    auto it = find_if(begin(storage), end(storage), [date](const pair<Date, set<string>>& p){return p.first > date;});
    auto it = storage.upper_bound(date);
    
    if (it == begin(storage))
    {
	return "No entries";
    }
    else
    {
	const auto it1 = prev(it);
	const auto& date_ = it1->first;
	return date_.toString() + " " + history.at(date_).back();
    }
}
