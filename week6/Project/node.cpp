#include "node.h"

int DateComparisonNode::Evaluate(const Date& date, const string& event) const
{
    if (cmp_ == Comparison::NotEqual)
    {
	return date != date_;
    }
    else if (cmp_ == Comparison::Equal)
    {
	return date == date_;
    }
    else if (cmp_ == Comparison::Less)
    {
	return date < date_;
    }
    else if (cmp_ == Comparison::LessOrEqual)
    {
	return date <= date_;
    }
    else if (cmp_ == Comparison::Greater)
    {
	return date > date_;
    }
    else if (cmp_ == Comparison::GreaterOrEqual)
    {
	return date >= date_;
    }
    return 0;
}

int EventComparisonNode::Evaluate(const Date& date, const string& event) const
{
    if (cmp_ == Comparison::NotEqual)
    {
	return event_ != event;
    }
    else if (cmp_ == Comparison::Equal)
    {
	return  event_ == event;
    }
    else if (cmp_ == Comparison::Less)
    {
	return event < event_;
    }
    else if (cmp_ == Comparison::LessOrEqual)
    {
	return event <= event_;
    }
    else if (cmp_ == Comparison::Greater)
    {
	return event > event_;
    }
    else if (cmp_ == Comparison::GreaterOrEqual)
    {
	return event >= event_;
    }
    return 0;
    
}

int LogicalOperationNode::Evaluate(const Date& date, const string& event) const
    {
	if (op_ == LogicalOperation::And)
	{
	    return left_->Evaluate(date, event) && right_->Evaluate(date, event);
	}
	else
	{
	    return left_->Evaluate(date, event) || right_->Evaluate(date, event);
	}
	return 0;
    };
