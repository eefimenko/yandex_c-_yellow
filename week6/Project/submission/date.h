#pragma once
#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>

using namespace std;

class Date {
public:
    Date(int year_, int month_, int day_);
    Date(const string& s);
    
    int GetYear() const;
    int GetMonth() const;
    int GetDay() const;
    
    // convert into a number of days since epoch
    int GetNumberOfDays() const;
    
    string toString() const;
        
private:
    int year;
    int month;
    int day;
};

bool operator<(const Date& lhs, const Date& rhs);
bool operator>(const Date& lhs, const Date& rhs);
bool operator<=(const Date& lhs, const Date& rhs);
bool operator>=(const Date& lhs, const Date& rhs);
bool operator!=(const Date& lhs, const Date& rhs);
bool operator==(const Date& lhs, const Date& rhs);

Date ParseDate(istream& is);

