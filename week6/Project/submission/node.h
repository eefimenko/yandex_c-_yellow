#pragma once
#include <string>
#include <memory>
#include "date.h"

using namespace std;

enum class Comparison
{
    Less, LessOrEqual, Greater, GreaterOrEqual, Equal, NotEqual,
};

enum class LogicalOperation
{
    And, Or,
};

class Node {
public:
    virtual int Evaluate(const Date& date, const string& event) const
    {
	return 0;
    };
};

class EmptyNode : public Node {
public:
    EmptyNode(){};
    int Evaluate(const Date& date, const string& event) const override
    {
	return 1;
    };
};

class EventComparisonNode : public Node
{
public:
EventComparisonNode(const Comparison& cmp, const string& value): event_(value), cmp_(cmp){};
    int Evaluate(const Date& date, const string& event) const override;
private:
    const string event_;
    const Comparison cmp_;
};

class DateComparisonNode : public Node
{
public:
DateComparisonNode(const Comparison& cmp, const string& value) : date_(value), cmp_(cmp){};
DateComparisonNode(const Comparison& cmp, const Date& date) : date_(date), cmp_(cmp){};
    int Evaluate(const Date& date, const string& event) const override;
    
private:
    const Date date_;
    const Comparison cmp_;
};

class LogicalOperationNode : public Node
{
public:
LogicalOperationNode(const LogicalOperation& op, shared_ptr<Node> left, shared_ptr<Node> right) :
    left_(left),
	right_(right),
	op_(op)	
	{};
    int Evaluate(const Date& date, const string& event) const override;
private:
    shared_ptr<Node> left_, right_;
    LogicalOperation op_;
};
