#include "database.h"
#include <algorithm>

void remove_duplicates(vector<string>& v)
{
    auto end = v.end();
    for (auto it = v.begin(); it != end; ++it) {
        end = remove(it + 1, end, *it);
    }

    v.erase(end, v.end());
}

void Database::Add(const Date& date, const string& event)
{
    storage[date].insert(event);
    history[date].push_back(event);   
}

void Database::Print(ostream& out) const
{
//    cout << "----" << endl;
    for (const auto& p: history)
    {
	string date = p.first.toString();
	auto events = p.second;
	
	reverse(begin(events),end(events));
	remove_duplicates(events);
	reverse(begin(events),end(events));
	
	for (const auto& v: events)
	{
	    out << date << " " << v << endl;
	}
    }
/*    for (const auto& p: history)
    {
	string date = p.first.toString();
	for (const auto& v: p.second)
	{
	    out << "h: " << date << " " << v << endl;
	}
	}
	cout << "----" << endl;*/
}

int Database::RemoveIf(function<bool(const Date& date, const string& event)> lambda)
{
    int n = 0;

    for (auto it0 = begin(storage); it0 != end(storage);)
    {
	auto& date = it0->first;
	auto& events = it0->second;
	
	for (auto it = begin(events); it != end(events);)
	{
	    auto& event = *it;
	    if (lambda(date, event))
	    {
/*		for (const auto& e: history[date])
		{
		    cout << "RI before" << e << " " << event << endl;
		    }*/
		auto it_ = remove(begin(history[date]),end(history[date]),event);
		history[date].erase(it_, end(history[date]));

		it = events.erase(it);
		n++;
	    }
	    else
	    {
		it++;
	    }
	}

	if (history[date].empty())
	{
	    history.erase(date);
	}
	if (storage[date].empty())
	{
	    it0 = storage.erase(it0);
	}
	else
	{
	    it0++;
	}
    }
    return n;
}

vector<string> Database::FindIf(function<bool(const Date& date, const string& event)> lambda)
{
    vector<string> res;
    for (const auto& p: history)
    {
	auto& date = p.first;
	auto events = p.second;
	
	reverse(begin(events),end(events));
	remove_duplicates(events);
	reverse(begin(events),end(events));

	for (const auto& event: events)
	{
	    if (lambda(date, event))
	    {
		res.push_back(date.toString() + " " + event);
	    }
	}
    }
    return res;
}

string Database::Last(const Date& date)
{
//    auto it = find_if(begin(storage), end(storage), [date](const pair<Date, set<string>>& p){return p.first > date;});
    auto it = storage.upper_bound(date);
    
    if (it == begin(storage))
    {
	return "No entries";
    }
    else
    {
	const auto it1 = prev(it);
	const auto& date_ = it1->first;
//	const auto& events_ = it1->second;
	return date_.toString() + " " + *prev(end(history[date_]));
    }
}
