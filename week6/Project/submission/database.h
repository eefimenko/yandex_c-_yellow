#pragma once
#include "date.h"
#include <map>
#include <set>
#include <string>
#include <iostream>
#include <functional>
#include <vector>

class Database {
public:
    void Add(const Date& date, const string& event);
    void Print(ostream& out) const;
    int RemoveIf(function<bool(const Date& date, const string& event)> lambda);
    vector<string> FindIf(function<bool(const Date& date, const string& event)> lambda);
    string Last(const Date& date);
private:
    map<Date,set<string>> storage;
    map<Date,vector<string>> history;
};
