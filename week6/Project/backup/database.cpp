#include "database.h"
#include <algorithm>

void Database::Add(const Date& date, const string& event)
{
    storage[date].insert(event);
    history[date].push_back(event);   
}

void Database::Print(ostream& out) const
{
//    cout << "----" << endl;
    for (const auto& p: storage)
    {
	string date = p.first.toString();
	for (const auto& v: p.second)
	{
	    out << date << " " << v << endl;
	}
    }
/*    for (const auto& p: history)
    {
	string date = p.first.toString();
	for (const auto& v: p.second)
	{
	    out << "h: " << date << " " << v << endl;
	}
	}*/
//    cout << "----" << endl;
}

int Database::RemoveIf(function<bool(const Date& date, const string& event)> lambda)
{
    int n = 0;
    for (auto& p: storage)
    {
	auto& date = p.first;
	auto& events = p.second;
	for (const auto& event: events)
	{
	    if (lambda(date, event))
	    {
/*		for (const auto& e: history[date])
		{
		    cout << "RI before" << e << " " << event << endl;
		    }*/
		auto it = remove(begin(history[date]),end(history[date]),event);
		history[date].erase(it, end(history[date]));
/*		for (const auto& e: history[date])
		{
		    cout << "RI after" << e << endl;
		    }*/
		events.erase(event);
		n++;
	    }
	}
	if (history[date].empty())
	{
	    history.erase(date);
	}
	if (storage[date].empty())
	{
	    storage.erase(date);
	}
    }
    return n;
}

vector<string> Database::FindIf(function<bool(const Date& date, const string& event)> lambda)
{
    vector<string> res;
    for (const auto& p: storage)
    {
	string date = p.first.toString();
	const auto& events = p.second;
	for (const auto& event: events)
	{
	    if (lambda(date, event))
	    {
		res.push_back(date + " " + event);
	    }
	}
    }
    return res;
}

string Database::Last(const Date& date)
{
    const auto it = find_if(begin(storage), end(storage), [date](const pair<Date, set<string>>& p){return p.first > date;});
//    cout << (it->first).toString() + " " + *begin(it->second); 
    if (it == begin(storage))
    {
	return "No entries";
    }
    else
    {
//	cout << begin(storage) - it << endl;
	const auto it1 = prev(it);
	const auto& date_ = it1->first;

	return date_.toString() + " " + *prev(end(history[date_]));
    }
//    return "hhh";
}
