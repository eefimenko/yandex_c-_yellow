#include "date.h"
#include <iostream>
#include <sstream>

void PeekAndSkip(istream& in, const string& s)
{
    char peek = in.peek(); 
    if (peek != '-')
    {
	throw domain_error(s);
    }
    in.ignore(1);
}

void PeekAndSkipLast(istream& in, const string& s)
{
    char peek = in.peek(); 
    if (peek != ' ' && peek != EOF)
    {
	throw domain_error(s);
    }
    in.ignore(1);
}

Date::Date(int year_=1, int month_=1, int day_=0)
{
    if (month_ < 1 || month_ > 12)
    {
	stringstream ss;
	ss << "Month value is invalid: " << month_;
	throw invalid_argument(ss.str());
    }
    if (day_ < 1 || day_ > 31)
    {
	stringstream ss;
	ss << "Day value is invalid: " << day_;
	throw invalid_argument(ss.str());
    }
    day = day_;
    month = month_;
    year = year_;
}
    
Date::Date(const string& s)
{
    string message = "Wrong date format: " + s;
    stringstream ss(s);
    int year_, month_, day_;
    ss >> year_;
    PeekAndSkip(ss, message);
    ss >> month_;
    if (month_ < 1 || month_ > 12)
    {
	stringstream ss;
	ss << "Month value is invalid: " << to_string(month_);
	throw invalid_argument(ss.str());
    }
    PeekAndSkip(ss, message);
    ss >> day_;
    if (day_ < 1 || day_ > 31)
    {
	stringstream ss;
	ss << "Day value is invalid: " << to_string(day_);
	throw invalid_argument(ss.str());
    }
    PeekAndSkipLast(ss, message);
    day = day_;
    month = month_;
    year = year_;
}
    
int Date::GetYear() const
{
    return year;
};
int Date::GetMonth() const
{
    return month;
};
int Date::GetDay() const
{
    return day;
};

// convert into a number of days since epoch
int Date::GetNumberOfDays() const
{
    return year*12*31 + (month-1)*31 + day;	
}

string Date::toString() const
{
    stringstream ss;
    ss << setw(4) << setfill('0') << year << "-"
       << setw(2) << setfill('0') << month << "-"
       << setw(2) << setfill('0') << day;
    return ss.str();
}

bool operator<(const Date& lhs, const Date& rhs)
{
    return lhs.GetNumberOfDays() < rhs.GetNumberOfDays();
};
bool operator>(const Date& lhs, const Date& rhs)
{
    return lhs.GetNumberOfDays() > rhs.GetNumberOfDays();
};
bool operator<=(const Date& lhs, const Date& rhs)
{
    return lhs.GetNumberOfDays() <= rhs.GetNumberOfDays();
};
bool operator>=(const Date& lhs, const Date& rhs)
{
    return lhs.GetNumberOfDays() >= rhs.GetNumberOfDays();
};
bool operator!=(const Date& lhs, const Date& rhs)
{
    return lhs.GetNumberOfDays() != rhs.GetNumberOfDays();
};
bool operator==(const Date& lhs, const Date& rhs)
{
    return lhs.GetNumberOfDays() == rhs.GetNumberOfDays();
};

Date ParseDate(istream& is)
{
    string date_s;
    getline(is, date_s, ' ');
    getline(is, date_s, ' ');
//    cout << date_s << endl;
    return Date(date_s);
}
