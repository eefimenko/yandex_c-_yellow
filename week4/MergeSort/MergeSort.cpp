#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

template <typename RandomIt>
void MergeSort(RandomIt range_begin, RandomIt range_end)
{
    int n = range_end - range_begin;
    if (n < 2)
    {
	return;
    }
    vector<typename RandomIt::value_type> elements(range_begin, range_end);
    RandomIt middle = begin(elements) + n/2;
    MergeSort(begin(elements), middle);
    MergeSort(middle, end(elements));
    merge(begin(elements), middle, middle, end(elements), range_begin);
}

int main() {
    vector<int> v = {6, 4, 7, 6, 4, 4, 0, 1, 2};
    for (int x : v) {
	cout << x << " ";
    }
    cout << endl;
    MergeSort(begin(v), end(v));
    for (int x : v) {
	cout << x << " ";
    }
    cout << endl;
    return 0;
} 
