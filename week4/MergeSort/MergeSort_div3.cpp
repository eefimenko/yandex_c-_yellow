#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

template <typename RandomIt>
void MergeSort(RandomIt range_begin, RandomIt range_end)
{
    int n = range_end - range_begin;
    if (n < 3)
    {
	return;
    }
    vector<typename RandomIt::value_type> elements(range_begin, range_end);
    RandomIt middle1 = begin(elements) + n/3;
    RandomIt middle2 = middle1 + n/3;
    MergeSort(begin(elements), middle1);
    MergeSort(middle1, middle2);
    MergeSort(middle2, end(elements));
    vector<typename RandomIt::value_type> tmp;
    merge(begin(elements), middle1, middle1, middle2, back_inserter(tmp));
    merge(begin(tmp), end(tmp), middle2, end(elements), range_begin);
}

int main() {
    vector<int> v = {6, 4, 7, 6, 4, 4, 0, 1, 2};
    for (int x : v) {
	cout << x << " ";
    }
    cout << endl;
    MergeSort(begin(v), end(v));
    for (int x : v) {
	cout << x << " ";
    }
    cout << endl;
    return 0;
} 
