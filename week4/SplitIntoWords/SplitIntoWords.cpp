#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <set>

using namespace std;

vector<string> SplitIntoWords(const string& s)
{
    vector<string> words;
    auto it = begin(s);
    auto it_prev= begin(s);
//    int h;
    while(it != end(s))
    {
	if (it != begin(s))
	{
	    it++;
	}
	it_prev = it;
	it = find_if(it, end(s), [](const char& c){ return c == ' ';});
//	cout << string(it_prev,it) << endl;
//	cin >> h;
	words.push_back(string(it_prev,it));
    }
    return words;
}

int main() {
    string s = "C Cpp Java Python";

    vector<string> words = SplitIntoWords(s);
    cout << words.size() << " ";
    for (auto it = begin(words); it != end(words); ++it) {
	if (it != begin(words)) {
	    cout << "/";
	}
	cout << *it;
    }
    cout << endl;
  
    return 0;
}
