#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;
void PrintVectorPart(const vector<int>& numbers)
{
    auto it = find_if(begin(numbers), end(numbers), [] (const int& x) {return x < 0;});
    if (it == end(numbers))
    {
	auto reversed_ = numbers;
	reverse(begin(reversed_), end(reversed_));
	for (const auto& v: reversed_)
	{
	    cout << v << " ";
	}
    }
    else if (it != begin(numbers))
    {
	vector<int> reversed_ = numbers;
	auto it__ = find_if(begin(reversed_), end(reversed_), [] (int& x) {return x < 0;}); 
	reverse(begin(reversed_), it__);
	for (auto it_ = begin(reversed_); it_ < it__; it_++)
	{
	    cout << *it_ << " ";
	}
    }
    cout << endl;
}

int main() {
    PrintVectorPart({6, 1, 8, -5, 4});
    PrintVectorPart({-6, 1, 8, -5, 4});  // ничего не выведется
    PrintVectorPart({6, 1, 8, 5, 4});
    return 0;
}
