#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <set>

using namespace std;
template<typename T>
vector<T> FindGreaterElements(const set<T>& elements, const T& border)
{
/*    cout << "Elements:" << endl;
    for (const auto& e : elements)
    {
	cout << e << endl;
    }
    cout << "End" << endl;*/
    vector<T> result;
    auto it = find_if(begin(elements), end(elements), [&] (const T& elem){return elem > border;});
//    if (it != end(elements) && it != begin(elements))
//    {
//	it++;
//    }
    for (auto it_ = it; it_ != end(elements); it_++)
    {
	result.push_back(*it_);
    }
    return result;
}

int main() {
    for (int x : FindGreaterElements(set<int>{1, 5, 5, 7, 3}, 5)) {
	cout << x << " ";
    }
    cout << endl;
    for (int x : FindGreaterElements(set<int>{1, 5, 5, 7, 3}, 0)) {
	cout << x << " ";
    }
    cout << endl;
    
    string to_find = "Python";
    cout << FindGreaterElements(set<string>{"C", "C++"}, to_find).size() << endl;
    return 0;
}
