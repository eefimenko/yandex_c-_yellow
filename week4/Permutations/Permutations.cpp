#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;
#include <algorithm>
using namespace std;

int f()
{ 
    static int i = 1;
    return i++;
}

void printVector(const vector<int>& v)
{
    for (const auto& el: v)
    {
	cout << el << " ";
    }
    cout << endl;
}

int main() {
    int N;
    cin >> N;
    vector<int> v(N);
    generate(v.rbegin(), v.rend(), f);
    
    do {
        printVector(v);
    } while(prev_permutation(v.begin(), v.end()));
    return 0;
}

