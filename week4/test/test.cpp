#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <set>

using namespace std;

bool f(int x)
{
    return x > 4;
}

void printVector(const vector<int> v)
{
    for (const int& el: v)
    {
	cout << el << " ";
    }
    cout << endl;
}

int main() {
    set<int> s = {1,2,3,4,5,6,7,8};
//    vector<int> v;
    vector<int> correct = {1,2,3,4};
/*  a) */
    int i = 0;
    {
	vector<int> v;    
	remove_copy_if(begin(s), end(s), back_inserter(v),
		       [](int x) { return !f(x); });
	sort(begin(v), end(v));
	printVector(v);
	cout << ++i << " " << (v == correct) << endl;
    }
    {
	vector<int> v;
//	auto it = partition(begin(s), end(s), f);
//	copy(it, end(s), back_inserter(v));
	sort(begin(v), end(v));
	printVector(v);
	cout << ++i << " " << (v == correct) << endl;
    }
    {
	vector<int> v;
	copy_if(begin(s), end(s), back_inserter(v), f);
	sort(begin(v), end(v));
	printVector(v);
	cout << ++i << " " << (v == correct) << endl;
    }
    {
	vector<int> v;
	v.assign(begin(s), end(s));
	sort(begin(v), end(v),
	     [](int lhs, int rhs) { return f(lhs) > f(rhs); });
	auto it = partition_point(begin(v), end(v), f);
	v.erase(it, end(v));
	sort(begin(v), end(v));
	printVector(v);
	cout << ++i << " " << (v == correct) << endl;
    }
    {
	vector<int> v;
	v.assign(begin(s), end(s));
	auto it = partition(begin(v), end(v), f);
	v.erase(it, end(v));
	sort(begin(v), end(v));
	printVector(v);
	cout << ++i << " " << (v == correct) << endl;
    }
    {
	vector<int> v;
	v.assign(begin(s), end(s));
	auto it = partition(begin(v), end(v), f);
	v.erase(begin(v), it);
	sort(begin(v), end(v));
	printVector(v);
	cout << ++i << " " << (v == correct) << endl;
    }
    {
	vector<int> v;
	v.assign(begin(s), end(s));
	auto it = remove_if(begin(v), end(v),
			    [](int x) { return !f(x); });
	v.erase(it, end(v));
	sort(begin(v), end(v));
	printVector(v);
	cout << ++i << " " << (v == correct) << endl;
    }
    {
	vector<int> v;
	vector<int> garbage;
	partition_copy(
	    begin(s), end(s),
	    back_inserter(garbage), back_inserter(v), f);
	v.assign(begin(s), end(s));
	sort(begin(v), end(v));
	printVector(v);
	cout << ++i << " " << (v == correct) << endl;
    }
    {
	vector<int> v;
	vector<int> garbage;
	partition_copy(
	    begin(s), end(s),
	    back_inserter(v), back_inserter(garbage), f);
	sort(begin(v), end(v));
	printVector(v);
	cout << ++i << " " << (v == correct) << endl;
    }
    {
	vector<int> v;
	v.assign(begin(s), end(s));
	auto it = remove_if(begin(v), end(v), f);
	v.erase(it, end(v));
	sort(begin(v), end(v));
	printVector(v);
	cout << ++i << " " << (v == correct) << endl;
    }
    {
	vector<int> v;
	v.assign(begin(s), end(s));
	sort(begin(v), end(v),
	     [](int lhs, int rhs) { return f(lhs) > f(rhs); });
	auto it = partition_point(begin(v), end(v), f);
	v.erase(begin(v), it);
	sort(begin(v), end(v));
	printVector(v);
	cout << ++i << " " << (v == correct) << endl;
    }
    {
	vector<int> v;
//	auto it = remove_if(begin(s), end(s),
//			    [](int x) { return !f(x); });
//	copy(it, end(s), back_inserter(v));
	sort(begin(v), end(v));
	printVector(v);
	cout << ++i << " " << (v == correct) << endl;
    }
    {
	vector<int> v;
	copy_if(begin(s), end(s), back_inserter(v),
		[](int x) { return !f(x); });
	sort(begin(v), end(v));
	printVector(v);
	cout << ++i << " " << (v == correct) << endl;
    }
    {
	vector<int> v;
	remove_copy_if(begin(s), end(s), back_inserter(v), f);
	sort(begin(v), end(v));
	printVector(v);
	cout << ++i << " " << (v == correct) << endl;
    }
    return 0;
}
