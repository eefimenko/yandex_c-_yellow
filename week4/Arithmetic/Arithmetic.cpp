#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <algorithm>
#include <deque>
using namespace std;

int precedence(const string& operation)
{
    if (operation == "+" || operation == "-")
    {
	return 0;
    }
    return 1;
}

int main() {
    int x, N;
    cin >> x >> N;
    deque<string> s {to_string(x)};
    int prev_p = -1,p;
    for (int i = 0; i < N; i++)
    {
	string operation;
	int number;
	cin >> operation >> number;
	p = precedence(operation);
	if (i == 0 || prev_p >= p)
	{
	    s.push_back(" " + operation + " " + to_string(number));
	}
	else
	{
	    s.push_front("(");
	    s.push_back(") " + operation + " " + to_string(number));
	}
	prev_p = p;
    }
    for (const auto& el: s)
    {
	cout << el;
    }
    cout << endl;
}
