#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Person {
public:
    Person(const string& name, const string& profession) :
	Name(name),
	Profession(profession)
    {
    };
    virtual void Walk(const string& destination) const
    {
	cout << Profession << ": " << Name << " walks to: " << destination << endl;
    }; 
    const string Name, Profession; 
};

class Student : public Person {
public:
    Student(const string& name, const string& favouriteSong) :
	Person(name, "Student"),
	FavouriteSong(favouriteSong)
    {
    };

    void Learn() const {
        cout << "Student: " << Name << " learns" << endl;
    }

    void Walk(const string& destination) const override{
        cout << "Student: " << Name << " walks to: " << destination << endl;
	cout << "Student: " << Name << " sings a song: " << FavouriteSong << endl;
    }

    void SingSong() const {
        cout << "Student: " << Name << " sings a song: " << FavouriteSong << endl;
    }

    const string FavouriteSong;
};


class Teacher : public Person {
public:

    Teacher(const string& name, const string& subject) :
	Person(name, "Teacher"),
	Subject(subject)
    {
    }

    void Teach() const {
        cout << "Teacher: " << Name << " teaches: " << Subject << endl;
    }

    const string Subject;
};


class Policeman : public Person{
public:
    Policeman(const string& name) :
	Person(name, "Policeman")
    {
    }

    void Check(const Person& t) const {
        cout << "Policeman: " << Name << " checks " << t.Profession << ". " << t.Profession <<"'s name is: " << t.Name << endl;
    }
};


void VisitPlaces(const Person& person, vector<string> places) {
    for (auto p : places) {
        person.Walk(p);
    }
}

int main() {
    Teacher t("Jim", "Math");
    Student s("Ann", "We will rock you");
    Policeman p("Bob");

    VisitPlaces(t, {"Moscow", "London"});
    p.Check(s);
    VisitPlaces(s, {"Moscow", "London"});
    return 0;
}


