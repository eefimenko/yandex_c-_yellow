#include <iostream>
#include <string>
#include <vector>

using namespace std;


class Person {
public:
    Person(const string& t, const string& name) : type(t) {
        Name = name;
    }

    const string& GetName() const {
        return Name;
    }

    const string& GetType() const {
        return type;
    }

    virtual void Walk(const string& destination) const {
        cout << GetType() << ": " << GetName() << " walks to: " << destination << endl;
    }

private:
    const string type;
    string Name;
};


class Student : public Person {
public:
    Student(const string& name, const string& favouriteSong) : Person("Student", name) {
        FavouriteSong = favouriteSong;
    }

    void Learn() const {
        cout << GetType() << ": " << GetName() << " learns" << endl;
    }

    void Walk(const string& destination) const override {
        Person::Walk(destination);
        SingSong();
    }

    void SingSong() const {
        cout << GetType() << ": " << GetName() << " sings a song: " << FavouriteSong << endl;
    }

private:
    string FavouriteSong;
};


class Teacher : public Person {
public:
    Teacher(const string& name, const string& subject) : Person("Teacher", name) {
        Subject = subject;
    }

    void Teach() const {
        cout << GetType() << ": " << GetName() << " teaches: " << Subject << endl;
    }

private:
    string Subject;
};


class Policeman : public Person {
public:
    Policeman(const string& name) : Person("Policeman", name) {};

    void Check(const Person& p) const {
        cout << GetType() << ": " << GetName() << " checks " << p.GetType() << ". "
             << p.GetType() << "'s name is: " << p.GetName() << endl;
    }
};


void VisitPlaces(const Person& t, const vector<string>& places) {
    for (auto& p : places) {
        t.Walk(p);
    }
}


int main() {
    Teacher t("Jim", "Math");
    Student s("Ann", "We will rock you");
    Policeman p("Bob");

    VisitPlaces(t, {"Moscow", "London"});
    p.Check(s);
    VisitPlaces(s, {"Moscow", "London"});
    return 0;
}
