#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Person {
public:
  Person(const string &name, const string &occupation)
      : name_(name), occupation_(occupation){};

  virtual void Walk(const std::string &destination) const {
    std::cout << Log() << " walks to: " << destination << std::endl;
  }

  string Log() const { return occupation_ + ": " + name_; }

  const string name_, occupation_;
};

class Student : public Person {
public:
  Student(const string &name, const string &favouriteSong)
      : Person(name, "Student"), favouriteSong_(favouriteSong) {}

  void Learn() { cout << "Student: " << name_ << " learns" << endl; }

  void Walk(const string &destination) const override {
    std::cout << Log() << " walks to: " << destination << std::endl;
    std::cout << Log() << " sings a song: " << favouriteSong_ << std::endl;
  }

  const string favouriteSong_;
};

class Teacher : public Person {
public:
  Teacher(const string &name, const string &subject)
      : Person(name, "Teacher"), subject_(subject) {}

  void Teach() { cout << Log() << " teaches: " << subject_ << endl; }

  const string subject_;
};

class Policeman : public Person {
public:
  Policeman(const string &name) : Person(name, "Policeman") {}

  void Check(const Person &per) const {
    cout << Log() << " checks " << per.occupation_ << ". " << per.occupation_
         << "'s name is: " << per.name_ << endl;
  }
};

void VisitPlaces(const Person &per, const vector<string> &places) {
  for (auto p : places) {
    per.Walk(p);
  }
}

int main() {
  Teacher t("Jim", "Math");
  Student s("Ann", "We will rock you");
  Policeman p("Bob");

  vector<string> cities = {"Moscow", "London"};
  VisitPlaces(t, cities);
  p.Check(s);
  VisitPlaces(s, cities);
  return 0;
}