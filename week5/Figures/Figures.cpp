#include <iostream>
#include <string>
#include <memory>
#include <cmath>
#include <vector>
#include <sstream>
#include <iomanip>

using namespace std;

class Figure
{
public:
    Figure(const string Name) : Name_(Name)
    {

    };
    virtual string Name() const = 0;
    virtual double Perimeter() const = 0;
    virtual double Area() const = 0;
    const string Name_;
};

class Triangle : public Figure
{
public:
    Triangle(const string& Name, double a, double b, double c) :
	Figure(Name), a_(a), b_(b), c_(c)
    {

    };
private:
    string Name() const
    {
	return Name_;
    }
    double Perimeter() const
    {
	return a_ + b_ + c_;
    }
    double Area() const
    {
	double p = Perimeter()/2.;
	return sqrt(p*(p-a_)*(p-b_)*(p-c_));
    }
    const double a_, b_, c_;
};

class Rect : public Figure
{
public:
    Rect(const string& Name, double w, double h) :
	Figure(Name), w_(w), h_(h)
    {

    };
private:
    string Name() const
    {
	return Name_;
    }
    double Perimeter() const
    {
	return 2.*(w_ + h_);
    }
    double Area() const
    {
	return w_*h_;
    }
    const double w_,h_;
};

class Circle : public Figure
{
public:
    Circle(const string& Name, double r) :
	Figure(Name), r_(r)
    {

    };
private:
    string Name() const
    {
	return Name_;
    }
    double Perimeter() const
    {
	return 2.*3.14*r_;
    }
    double Area() const
    {
	return 3.14*r_*r_;
    }
    const double r_;
};

shared_ptr<Figure> CreateFigure(istringstream& is)
{
    string type;
    is >> type;
    if (type == "CIRCLE")
    {
	double r;
	is >> r;
	return make_shared<Circle>("CIRCLE", r);
    }
    else if (type == "RECT")
    {
	double w,h;
	is >> w >> h;
	return make_shared<Rect>("RECT", w, h);
    }
    else if (type == "TRIANGLE")
    {
	double a,b,c;
	is >> a >> b >> c;
	return make_shared<Triangle>("TRIANGLE", a, b, c);
    }
}

int main() {
  vector<shared_ptr<Figure>> figures;
  for (string line; getline(cin, line); ) {
    istringstream is(line);

    string command;
    is >> command;
    if (command == "ADD") {
      figures.push_back(CreateFigure(is));
    } else if (command == "PRINT") {
      for (const auto& current_figure : figures) {
        cout << fixed << setprecision(3)
             << current_figure->Name() << " "
             << current_figure->Perimeter() << " "
             << current_figure->Area() << endl;
      }
    }
  }
  return 0;
}
