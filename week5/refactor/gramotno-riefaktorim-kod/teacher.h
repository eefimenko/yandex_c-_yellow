#pragma once

#include <string>
#include "person.h"

class Teacher : public Person {
public:

    Teacher(string name, string subject);
    void Teach() const;

private:
    string subject_;
};

