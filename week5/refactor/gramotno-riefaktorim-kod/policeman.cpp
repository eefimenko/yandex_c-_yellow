#include <string>
#include <iostream>

#include "person.h"
#include "policeman.h"


Policeman::Policeman(string name) : Person(name, "Policeman") {
}

void Policeman::Check(const Person& person) const {
    cout << Title() << " checks "
         << person.Type() << ". " << person.Type() << "'s name is: " << person.Name() << endl;
}