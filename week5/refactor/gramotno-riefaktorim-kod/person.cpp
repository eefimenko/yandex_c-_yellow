#include <string>
#include "person.h"

Person::Person(string name, string type) : name_(name), type_(type) {}

string Person::Name() const {
    return name_;
}

string Person::Type() const {
    return type_;
}

void Person::Walk(string destination) const {
    cout <<  Title() << " walks to: " << destination << endl;
}

string Person::Title() const {
    return type_ + ": " + name_;
}