#pragma once

#include <string>
#include <iostream>

using namespace std;

class Person {
public:
    Person(string name, string type);
    string Name() const;
    string Type() const;
    virtual void Walk(string destination) const;

protected:
    string Title() const;

private:
    const string name_;
    string type_;
};


