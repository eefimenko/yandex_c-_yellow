#include <iostream>
#include <string>
#include "person.h"
#include "student.h"

Student::Student(string name, string favouriteSong) : Person(name, "Student"), favourite_song_(favouriteSong) {
}

void Student::Walk(string destination) const {
    Person::Walk(destination);
    Student::SingSong();
}

void Student::SingSong() const {
    cout << Title() << " sings a song: " << favourite_song_ << endl;
}

void Student::Learn() const {
    cout << Title() << " learns" << endl;
}