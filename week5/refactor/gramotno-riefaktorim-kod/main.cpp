#include <iostream>
#include <string>
#include <vector>

#include "person.h"
#include "student.h"
#include "teacher.h"
#include "policeman.h"

using namespace std;

void VisitPlaces(const Person& t, vector<string> places) {
    for (const auto& p : places) {
        t.Walk(p);
    }
}

int main() {
    Teacher t("Jim", "Math");
    Student s("Ann", "We will rock you");
    Policeman p("Bob");
    s.Learn();
    t.Teach();
    VisitPlaces(t, {"Moscow", "London"});
    p.Check(s);
    VisitPlaces(s, {"Moscow", "London"});
    return 0;
}