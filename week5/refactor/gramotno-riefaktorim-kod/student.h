#pragma once

#include <iostream>
#include "person.h"

class Student : public Person {
public:
    Student(string name, string favouriteSong);
    void Learn() const;
    void Walk(string destination) const override;
    void SingSong() const;

private:
    string favourite_song_;
};
