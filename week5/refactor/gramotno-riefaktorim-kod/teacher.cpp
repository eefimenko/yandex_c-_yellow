#include <string>
#include <iostream>
#include "teacher.h"


Teacher::Teacher(string name, string subject) : Person(name, "Teacher"), subject_(subject) {
}

void Teacher::Teach() const {
    cout << Title() << " teaches: " << subject_ << endl;
}