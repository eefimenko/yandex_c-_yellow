#pragma once

#include <string>
#include <iostream>
#include "person.h"

class Policeman : public Person {
public:
    Policeman(string name);
    void Check(const Person& person) const;
};


